package bom

import (
	"fmt"
	"os"
	"testing"
)

func TestDecodeProduct(t *testing.T) {

	fd, err := os.Open("testdata/IDN60920.xml")
	if err != nil {
		err = fmt.Errorf("failed to load test data: %w", err)
		t.Fatal(err)
	}

	product, err := DecodeProduct(fd)
	if err != nil {
		err = fmt.Errorf("failed to decode test data: %w", err)
		t.Error(err)
	}

	t.Logf("%+v", product)

}
