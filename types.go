package bom

import "time"

type Source struct {
	Sender     string `xml:"sender"`
	Region     string `xml:"region"`
	Office     string `xml:"office"`
	Copyright  string `xml:"copyright"`
	Disclaimer string `xml:"disclaimer"`
}

type AMOC struct {
	Source      Source    `xml:"source"`
	Identifier  string    `xml:"identifier"`
	IssueTime   time.Time `xml:"issue-time-utc"`
	SentTime    time.Time `xml:"sent-time"`
	Status      string    `xml:"status"`
	Service     string    `xml:"service"`
	ProductType string    `xml:"product-type"`
	Phase       string    `xml:"phase"`
}

type Forecast struct {
	Text string `xml:",chardata"`
	Area []struct {
		Text           string `xml:",chardata"`
		Aac            string `xml:"aac,attr"`
		Description    string `xml:"description,attr"`
		Type           string `xml:"type,attr"`
		ParentAac      string `xml:"parent-aac,attr"`
		ForecastPeriod []struct {
			Chardata       string `xml:",chardata"`
			StartTimeLocal string `xml:"start-time-local,attr"`
			EndTimeLocal   string `xml:"end-time-local,attr"`
			StartTimeUtc   string `xml:"start-time-utc,attr"`
			EndTimeUtc     string `xml:"end-time-utc,attr"`
			Index          string `xml:"index,attr"`
			Text           []struct {
				Text string `xml:",chardata"`
				Type string `xml:"type,attr"`
			} `xml:"text"`
			Element []struct {
				Text  string `xml:",chardata"`
				Type  string `xml:"type,attr"`
				Units string `xml:"units,attr"`
			} `xml:"element"`
		} `xml:"forecast-period"`
	} `xml:"area"`
}

type Observations struct {
	Station []struct {
		WmoID              string  `xml:"wmo-id,attr"`
		BomID              string  `xml:"bom-id,attr"`
		Tz                 string  `xml:"tz,attr"`
		StnName            string  `xml:"stn-name,attr"`
		Height             float32 `xml:"stn-height,attr"`
		Type               string  `xml:"type,attr"`
		Lat                float32 `xml:"lat,attr"`
		Lon                float32 `xml:"lon,attr"`
		ForecastDistrictID string  `xml:"forecast-district-id,attr"`
		Description        string  `xml:"description,attr"`
		Period             struct {
			Index   string    `xml:"index,attr"`
			Time    time.Time `xml:"time-utc,attr"`
			WindSrc string    `xml:"wind-src,attr"`
			Level   struct {
				Index   string `xml:"index,attr"`
				Type    string `xml:"type,attr"`
				Element []struct {
					Units     string    `xml:"units,attr"`
					Type      string    `xml:"type,attr"`
					StartTime time.Time `xml:"start-time-utc,attr"`
					EndTime   time.Time `xml:"end-time-utc,attr"`
					Instance  string    `xml:"instance,attr"`
					Time      time.Time `xml:"time-utc,attr"`
				} `xml:"element"`
			} `xml:"level"`
		} `xml:"period"`
	} `xml:"station"`
}

type Product struct {
	Version      string       `xml:"version,attr"`
	AMOC         AMOC         `xml:"amoc"`
	Observations Observations `xml:"observations"`
	Forecast     Forecast     `xml:"forecast"`
}
