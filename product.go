package bom

import (
	"encoding/xml"
	"io"
)

func DecodeProduct(r io.Reader) (product *Product, err error) {
	d := xml.NewDecoder(r)
	product = &Product{}
	err = d.Decode(&product)
	return product, err
}
